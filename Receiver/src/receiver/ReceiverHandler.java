package receiver;


import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

public class ReceiverHandler {

	private final static String QUEUE_NAME = "MyOwnQueue5502";
	private final static String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com" ;
	private final static int PORT = 5672;

	private final static int QUALSER = 3000;

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setPort(PORT);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
					throws IOException {
		        final String message = new String(body, "UTF-8");
		        Thread thread = new Thread() {
		        	public void run() {
				        JSONObject json = new JSONObject(message);
				        long threadId = Thread.currentThread().getId();
			        	Worker worker = new Worker(threadId);
			        	worker.execute(json);
		        	}
		        };
		        thread.start();
			}
		};
		channel.basicConsume(QUEUE_NAME, true, consumer);
	}

}
