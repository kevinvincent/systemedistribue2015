/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package receiver;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONObject;

/**
 *
 * @author murat4u
 */
public class Worker {

	private final static String QUEUE_NAME = "QueueReturn";
	private final static String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com" ;
	private final static int PORT = 5672;
    private long id;
    
	public Worker(long threadId) {
		this.id = threadId; 
	}

	public void execute(JSONObject json) {
		try {
			int time = json.getInt("time");
			String IdMessage = json.getString("idMessage");
			String IdClient = json.getString("idClient");
			Thread.sleep(time);
			System.out.println("The worker " + this.id + " processed the client " + IdClient + " for the message " + IdMessage);
			try {
				json.put("IdCoworker", this.id);
				sendAck(json);
			} catch (IOException | TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sendAck(JSONObject json) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setPort(PORT);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.queueDeclare(json.getString("idClient"), false, false, false, null);
		channel.basicPublish("", json.getString("idClient"), null, json.toString().getBytes());
		channel.close();
		connection.close();
	}
}
