package client;
import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import callwebservice.MessageService;
import callwebservice.MessageServiceService;


public class Emetteur {
	
	private static String STOP = "stop";
	private final static String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com" ;
	private final static int PORT = 5672;
	  
	public static void main(String[] args) throws IOException, TimeoutException {
		boolean again = true;
		String input = "";
		MessageServiceService service = new MessageServiceService();
		MessageService messageService = service.getMessageServicePort();
		boolean first = true;
	    final UUID idClient = UUID.randomUUID();
		while (again) {
			System.out.println("Send the time you would like to wait\n>");
			Scanner sc = new Scanner(System.in);
			input = sc.nextLine();
			if (!STOP.equals(input)) {
				try {
					int time = Integer.parseInt(input);
					JSONObject jo = new JSONObject();
				    UUID idMessage = UUID.randomUUID();
					jo.put("idClient", idClient.toString());
					jo.put("idMessage", idMessage.toString());
					jo.put("time", time);
					String mes = jo.toString();
					messageService.transfer(mes);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			} else {
				again = false;
				System.out.println("\nThe service is finished ! We Hope you enjoyed our service's quality !");
			}
			if (first) {
				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost(HOST);
				factory.setPort(PORT);
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();
				channel.queueDeclare(idClient.toString(), false, false, false, null);
				Consumer consumer = new DefaultConsumer(channel) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
							throws IOException {
				        String message = new String(body, "UTF-8");
						JSONObject jso = new JSONObject(message);
						String IdReturnclient = jso.getString("idClient");
						if (idClient.toString().equals(IdReturnclient)) {
							System.out.println("The message " + jso.getString("idMessage") 
									+ " \nwas processed by the coworker " + jso.getInt("IdCoworker"));
						}
					}
				};
				channel.basicConsume(idClient.toString(), true, consumer);
				first = false;
			}
		}
	}

}
