package service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeoutException;

import javax.json.JsonObject;
import javax.jws.WebService;

import org.json.JSONObject;

@WebService
public class MessageService {
	
	  private final static String QUEUE_NAME = "MyOwnQueue5502";
	  private final static String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com";
	  private final static int PORT = 5672;
	  
	public String transfer(String json) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();		
	    factory.setHost(HOST);
	    factory.setPort(PORT);
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();
	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	    channel.basicPublish("", QUEUE_NAME, null, json.getBytes());
	    channel.close();
	    connection.close();
		return "send";
	}
}
